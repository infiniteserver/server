compile_gradle () { # fetch_git_repo_and_compile_gradle sounded too long
  git clone --depth 1 "$@" mod_files
  cd mod_files/

  ./gradlew --no-daemon build
  cp "$(ls build/libs/*.jar | awk '{print length($0)"\t"$0}' | sort -n | cut -f2- | head -n 1)" ../ # partially skidded from ai

  cd ../
  rm -rf mod_files/
}

mkdir fetched_mods
cd fetched_mods

# Fetch mods
compile_gradle 'https://code.chipmunk.land/infiniteserver/extras.git'

# Normalize filenames
mv extras-*.jar extras.jar
